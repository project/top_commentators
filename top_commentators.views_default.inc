<?php

function top_commentators_views_default_views() {
  $view = new view;
  $view->name = 'top_commentators';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'users';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Default', 'default');
  $handler->override_option('fields', array(
    'name' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
     'link_to_user' => 0,
     'overwrite_anonymous' => 1,
     'anonymous_text' => 'Guest',
     'exclude' => 0,
     'id' => 'name',
     'table' => 'users',
     'field' => 'name',
     'relationship' => 'none',
    ),
    'user_comments_number' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 1,
        'text' => '([user_comments_number])',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'exclude' => 0,
      'id' => 'user_comments_number',
      'table' => 'users',
      'field' => 'user_comments_number',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Default');
  $handler->override_option('items_per_page', 7);
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('style_options', array(
    'type' => 'ul',
  ));
  $handler = $view->new_display('block', 'Top Commentators', 'block_1');
  $handler->override_option('title', 'Top Commentators');
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);

  return array('top_commentators' => $view);
}
