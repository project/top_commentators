<?php

function top_commentators_views_data() {
  $data['users']['user_comments_number'] = array(
    'title' => t('Comments number'),
    'help' => t('Retrieve number of users content.'),
    'field' => array(
      'handler' => 'top_commentators_views_handler_field_user_comments_number',
    ),
    'filter' => array(
			'handler' => 'views_handler_filter_numeric',
    )
  );
  return $data;
}


function top_commentators_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'top_commentators') .'/views',
    ),
    'handlers' => array(
      'top_commentators_views_handler_field_user_comments_number' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
* Implementation of hook_views_pre_render().
*/
function top_commentators_views_pre_render(&$view) {

  if ($view->name == 'top_commentators') {
    drupal_add_css(drupal_get_path('module', 'top_commentators') . '/top_commentators.css');
  }
}
